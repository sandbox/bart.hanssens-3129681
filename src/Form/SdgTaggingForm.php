<?php

namespace Drupal\sdg_tagging\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SdgTaggingForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sdg_tagging_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('sdg_tagging.settings');

    $form['default_region'] = array(
     '#type' => 'select',
     '#title' => $this->t('Default country or region:'),
     '#options' => sdg_tagging_vocToOptions('sdg_nuts'),
     '#default_value' => $config->get('default_region'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('sdg_tagging.settings');
    $config->set('default_region', $form_state->getValue('default_region'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sdg_tagging.settings',
    ];
  } 
}
