<?php

/**
 * @file
 * Contains \Drupal\sdg_tagging\Plugin\Field\FieldWidget\SdgTagWidget
 */

namespace Drupal\sdg_tagging\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Plugin implementation of the SGD Tag widget.
 *
 * @FieldWidget( 
 *   id = "sdgtag_widget",
 *   module = "sdg_tagging",
 *   label = @Translation("SDG Tag widget"),
 *   field_types = { "sdgtag" }
 * )
 */

class SdgTagWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $defarea = \Drupal::config('sdg_tagging.settings')->get('default_region');
    $element['region'] = array(
      '#type' => 'select',
      '#title' => t('Country or region'),
      '#title_display' => 'invisible',
      '#options' => sdg_tagging_vocToOptions('sdg_nuts'),
      '#default_value' => isset($items[$delta]->region) ? $items[$delta]->region: $defarea
    );
    $element['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#title_display' => 'invisible',
      '#options' => sdg_tagging_vocToOptions('sdg_type'),
      '#default_value' => isset($items[$delta]->type) ? $items[$delta]->type : ''
    );
    $element['annex'] = array(
      '#type' => 'select',
      '#title' => t('Annex'),
      '#title_display' => 'invisible',
      '#options' => sdg_tagging_vocToOptions('sdg_annex'),
      '#default_value' => isset($items[$delta]->annex) ? $items[$delta]->annex : ''
    );

    $element += array(
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('container-inline')),
    );
    return $element;
  }
} 

