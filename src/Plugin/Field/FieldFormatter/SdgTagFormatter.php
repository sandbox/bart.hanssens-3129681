<?php

/**
 * @file
 * Contains \Drupal\sgd_tagging\Plugin\Field\FieldFormatter\SdgTagFormatter
 */

namespace Drupal\sdg_tagging\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of Sdg Tag formatter.
 *
 * @FieldFormatter(
 *   id = "sdgtag_formatter",
 *   module = "sdg_tagging",
 *   label = @Translation("SDG Tag formatter"),
 *   field_types = { "sdgtag" }
 * )
 */
class SdgTagFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $markup = 'SDG ' . $item->region . ' ' . $item->type;
      if (isset($langcode)) {
        $markup .= ' ' . strtoupper($langcode);
      }
      if (!empty($item->annex)) {
        $markup .= ' ' . $item->annex;
      }

      $elements[$delta] = array(
	'#type' => 'html_tag',
        '#tag' => 'span',
	'#value' => $markup,
	'#attributes' => array('data-sdg' => $markup)
      );
    }
    return $elements;
  }
}
