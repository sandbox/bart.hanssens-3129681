<?php

/**
 * @file
 * Contains \Drupal\sdg_tagging\Plugin\Field\FieldType\SdgTagItem
 */

namespace Drupal\sdg_tagging\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the SDG Tag field type.
 *
 * @FieldType(
 *   id = "sdgtag",
 *   label = @Translation("SDG Tag"),
 *   module = "sdg_tagging",
 *   description = @Translation("Stores SDG info as metadata field"),
 *   default_widget = "sdgtag_widget",
 *   default_formatter = "sdgtag_formatter"
 *  )
 */
class SdgTagItem extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
	'region' => 
          array('type' => 'varchar', 'length' => 8, 'not null' => TRUE),
	'type' => 
          array('type' => 'char', 'length' => 1, 'not null' => TRUE),
	'annex' => 
          array('type' => 'varchar', 'length' => 4, 'not null' => FALSE),
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $region = $this->get('region')->getValue();
    $type = $this->get('type')->getValue();
    $annex = $this->get('annex')->getValue();

   return empty($annex);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $props = [];

    $props['region'] = DataDefinition::create('string')
      ->setLabel(t('Region'))->setDescription(t('Country or region'));

    $props['type'] = DataDefinition::create('string')
      ->setLabel(t('Type'))->setDescription(t('Info/Procedure/Assistance'));

    $props['annex'] = DataDefinition::create('string')
      ->setLabel(t('Annex'))->setDescription(t('Part of the annex'));

    return $props;
  } 
}

